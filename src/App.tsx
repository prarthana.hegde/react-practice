import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route,Link,Switch,Redirect} from "react-router-dom";
import SignUpPage from '../src/pages/SignUpPage';
import FormPage from '../src/pages/FormPage';
import MainPage from '../src/pages/MainPage';
import ErrorPage from '../src/pages/ErrorPage';
import AboutPage from '../src/pages/AboutPage';
import CoursesPage from './pages/CoursesPage';
import HelpPage from '../src/pages/HelpPage';


function App() {
  return (
    <Router>
    <div className="App">
    <Switch>
      <Route exact path="/" component={SignUpPage}/>
      <Route exact path="/404" component={ErrorPage}/>
      <Route path="/form" component={FormPage} />
      <Route path="/main" component={MainPage}/>
      <Route path="/about" component={AboutPage}/>
      <Route path="/courses" component={CoursesPage}/>
      <Route path="/help" component={HelpPage}/>
      <Redirect to="/404"/>
     
    </Switch>
    





    </div>
  </Router>
  );
}

export default App;
