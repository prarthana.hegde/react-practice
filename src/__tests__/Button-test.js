import React from 'react';
import { shallow, mount, render } from 'enzyme';
import Button from '../components/Button';


describe('A suite', function() {
    it('should render without throwing an error', function() {
      expect(shallow(<Button />).contains(<div className="foo">Bar</div>)).toBe(true);
    });
  
    it('should be selectable by class "foo"', function() {
      expect(shallow(<Button />).is('.foo')).toBe(true);
    });

})