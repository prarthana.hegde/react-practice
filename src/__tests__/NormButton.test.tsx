import React from 'react';
import {} from 'jest';
import { assert } from 'chai';
import {shallow} from 'enzyme';
import NormButton from '../components/NormButton';

describe('Button Component',()=> {
    it('Contains the text',()=> {
       const wrapper = shallow(<NormButton/>);
       const text = wrapper.find('div div');
       expect(text.text()).toBe('Click the button');
    });

    it('Contains valid className',()=> {
        const wrapper = shallow(<NormButton/>);
        const clasText = wrapper.find('.NormButtonclass');
        assert.equal(clasText.length, 1);
     });

     it('Contains the button element',()=> {
        const wrapper = shallow(<NormButton/>);
        expect(wrapper).toBeDefined();
     });
     
     it('should call mock function when button is clicked', () => {
        const mockFn = jest.fn();
        const tree = shallow(
            <button className="NormButtonclass" onClick={mockFn} />
        );
        tree.simulate('click');
        expect(mockFn).toHaveBeenCalled();
      });
    
})
