import * as React from 'react'
import DeleteIcon from '@material-ui/icons/Delete';
// import Icon from '@material-ui/core/Icon';
import { withKnobs, text, boolean, number } from "@storybook/addon-knobs";
 

// import { ExportIcon,IconButton } from '@commercetools-frontend/ui-kit';


export interface IProps {
    backgroundColor: string,
    disabled:boolean,
    onClick?: () => void,
    flag:boolean,
    
}


export default (props: IProps) => {
    const {backgroundColor,disabled,onClick,flag} = props;
    if(flag===true)
    return (
      <div>

            <button style={{backgroundColor}} disabled={disabled} onClick={onClick} >Button</button>
      </div>
     )

     else
     return(
      <button style={{backgroundColor}} disabled={disabled} onClick={onClick}> <DeleteIcon/> Button</button>
     )
   
}


