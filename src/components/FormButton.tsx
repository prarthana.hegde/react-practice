import React from 'react';
import {Form,FormGroup,Button,Label,Input} from 'reactstrap'

export interface IProps {
    children?: React.ReactNode,
    Buttontitle?:string;
    className?:any;
    onClick?:()=>void;
    type?:any;
    style?:any;
    ref?:any;
    disabled?:boolean
}

const FormButton: React.SFC<IProps> = (props) => {
    const{children,Buttontitle,style,disabled}=props;
    return(
        <Button className="ButtonClass" style={style} disabled={disabled}>
          {Buttontitle} 
        </Button>
    )
}


FormButton.defaultProps = {
    children: null,
   
  };
  export default FormButton;
  