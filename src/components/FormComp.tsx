import React from 'react';
import {Form,FormGroup,Button,Label,Input} from 'reactstrap';
import FormLabel from './FormLabel';
import FormButton from './FormButton';


export interface IProps {
    children?: React.ReactNode,
    name?:any,
    title?:any,
    type:any,
    id:string,
    placeholder:string,
    Buttontitle:string
}

const FormComp: React.SFC<IProps> = (props) => {
    const{children,name,title,type,id,placeholder,Buttontitle}=props;
    return(
        <div>
            <FormLabel name={name} title={title} type={type} id={id} placeholder={placeholder}></FormLabel>
            <FormButton Buttontitle={Buttontitle}></FormButton>
        </div>
    )
}


FormComp.defaultProps = {
    children: null,
   
  };
  export default FormComp;
  