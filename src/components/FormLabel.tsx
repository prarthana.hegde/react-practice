import React from 'react';
import {Form,FormGroup,Button,Label,Input} from 'reactstrap'

export interface IProps {
    children?: React.ReactNode,
    name?:any,
    title?:any,
    type?:any,
    id?:string,
    placeholder?:string;
    style?:any;
    
}

const FormLabel: React.SFC<IProps> = (props) => {
    const{children,name,title,type,id,placeholder,style}=props;
    return(
        <div>
            <Label name={name} style={style}>{title}</Label>
            <Input type={type} name={name} id={id} placeholder={placeholder} style={style}></Input>
        </div>
    )
}


FormLabel.defaultProps = {
    children: null,
   
  };
  export default FormLabel;
  