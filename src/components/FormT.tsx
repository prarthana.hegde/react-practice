import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { Link} from "react-router-dom";
import { useForm } from 'react-hook-form';
import FormButton from '../components/FormButton';

export interface IProps {
  style?:any;
  linkto?:any;
  
}

const FormT:React.FC<IProps> = (props)=> {
  const {style,linkto}=props
  const initialValues = {
    email: '',
  };

  const emailRegex = RegExp(
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
  );
  const { register, handleSubmit } = useForm();
  const [bstate, update] = useState(true);
  const onSubmit = (data:any) => {
    alert(JSON.stringify(data));
  };

  const validate=(e)=>{
    if(emailRegex.test(e.target.value)==true) {
      update(false);
    }
  }
 

  return (
    <div className="App">
      <form onSubmit={handleSubmit(onSubmit)}>
        

        <div>
          <label style={style}>Email</label>
          <input
            defaultValue={initialValues.email}
            name="email"
            placeholder="Email Id"
            type="email"
            ref={register}
            onChange={validate}
            style={style}
          />
        </div>
        <div>
        <label style={style}>Password</label>
          <input
            defaultValue={initialValues.email}
            name="password"
            placeholder="Password"
            type="password"
            ref={register}
            style={style}
          />
        </div>
        <Link to={linkto} >
            <FormButton Buttontitle="Submit" type="submit" disabled={bstate} style={style}/>
            </Link>
      </form>
    </div>
  );
}

export default FormT