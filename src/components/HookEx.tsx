import React, { useState } from 'react';

interface IProps {
  onClick?:()=>void;
  count ?:number;
  
}

export default (props:IProps) => {
  const {onClick,count} =props;

  return (
    <div>
      <button onClick={onClick}>Click </button>
      
    </div>
  );
}
