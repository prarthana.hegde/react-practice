import React from 'react';
import Header from './Header';
import Footer from './Footer';

export interface IProps {

}

const Layout: React.SFC<IProps> = (props) => {
    const{}=props;
    return(
        <div>
            <Header></Header>
            <Footer></Footer>
        </div>
    )
}


Layout.defaultProps = {
    children: null,
   
  };
  export default Layout;