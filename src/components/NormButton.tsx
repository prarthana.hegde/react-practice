import React, { useState }  from "react";

interface IProps {
    onClick?:()=>void;
    // count ?:number;
    
  }
  
 
    
const NormButton = (props:IProps) => {
    const {onClick} =props;
    return(
        <div>
            <button className="NormButtonclass" onClick={onClick} />
            <div>Click the button</div>
        </div>
    )
}

export default NormButton;