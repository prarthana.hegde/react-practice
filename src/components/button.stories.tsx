import React from "react";
import { storiesOf } from "@storybook/react";
import Button from "./Button";
import { action } from '@storybook/addon-actions';
import { withKnobs, object ,text,boolean} from '@storybook/addon-knobs';


const stories = storiesOf('Button',module);
stories.addDecorator(withKnobs);


    stories.add("Simple button",
        () => <Button backgroundColor={text("backgroundColor","White")} disabled={boolean("Disabled",false)} flag={true}/>
    )
    .add("different Color",
        () => <Button backgroundColor={text("backgroundColor","Cyan")} disabled={boolean("Disabled",false)} flag={true} />
    )

    .add("Disabled",
    () => <Button backgroundColor={text("backgroundColor","White")} disabled={boolean("Disabled",true)} flag={true} />
    )

    .add("Button with onClick action",
    () => <Button backgroundColor={text("backgroundColor","White")} disabled={boolean("Disabled",false)} flag={true} onClick={() => {
        alert(" This button has an onClick action");
      }}
    />
    )

    .add("Button with icon",
    () => <Button backgroundColor="White" disabled={false} flag={false}/>
    )
