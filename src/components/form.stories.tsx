import React,{useState} from "react";
import {Label} from 'reactstrap';
import {Link} from 'react-router-dom';
import { storiesOf } from "@storybook/react";
import FormLabel from "./FormLabel";
import FormButton from './FormButton';
import DatePickerT from './DatePickerT';

const divStyle={
    paddingLeft:'25rem',
    paddingRight:'28rem',
    
  }
  
  const divStyle2={
    marginTop:'.5rem',
    border: '.03px solid grey',
    height:'39.5rem',
    width:'100%'
  }
  
  const h2Style = {
    padding:'.6rem',
    paddingRight:'2rem'
  }
  
  const labelStyle = {
    width:'25rem',
    paddingTop:'.6rem',
    marginLeft:'2rem',
    marginRight:'5rem'
  }
  
  
  const buttonStyle = {
    width:'25rem',
    height:'2.6rem',
    marginTop:'15rem',
    marginRight:'1rem',
  }
  

storiesOf("Form", module)
.add("Form",
       ()=>  {
                const [startDate , setStartDate] = useState(new Date());
                const handleChange =(date : any)=>{
                  setStartDate(date);
                }
            return(
              
                <div >
                   <div >
                   <h2 >Enter your details</h2>
        
                 <FormLabel title="First Name" type="text" placeholder="Enter First Name"/>
        
                <FormLabel  title="Last Name" type="text" placeholder="Enter Last Name" />
                
        
                <Label >Date of Birth</Label><br/>
                <DatePickerT startDate={startDate}  onChange={handleChange} flag={1} style={{paddingLeft:'500rem'}} /><br/>
        
                <Label >Gender</Label><br/>
                <FormLabel type="radio" />
                <Label >Male</Label>
                <FormLabel type="radio"  />
                <Label >Female</Label>
        
                 <Label >Area of interest</Label>
                 <FormLabel type="radio"  /> 
                <Label >Front End Development</Label> 
                <FormLabel type="radio"  />
                <Label>Python</Label>
                <FormLabel type="radio"  />
                <Label >Networking</Label> 
                <FormLabel  type="radio"/> 
                <Label >C,C++,Java</Label>  
        
               
                     <FormButton type="submit" Buttontitle="Submit"/>
                   
                   </div>
                
          </div>
              
            )
        }
        
)