import React from 'react';
import { storiesOf } from "@storybook/react";
import {Form,FormGroup,Button,Label,Input} from 'reactstrap';
import FormLabel from './FormLabel';
import FormButton from './FormButton';
import FormComp from './FormComp';

storiesOf("Form Composite Component", module)
    .add("Form Composite Component",
       ()=> {
          return(
            <div>
            <FormComp name="exampleEmail" title="Enter Email"  type="password" id="password" placeholder="Enter the password" Buttontitle="Submit"></FormComp>
            </div>
          )
       }
    )
