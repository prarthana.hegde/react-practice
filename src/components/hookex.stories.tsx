import React, { useState } from 'react';
import { storiesOf } from "@storybook/react";
import HookEx from './HookEx';

export interface State {
    onClick: ()=>void;
}

storiesOf("HooKEx", module)
    .add("Hooks",
       ()=> {
        const [count, setCount] = useState(0);
        return(
            <div>
                 <HookEx onClick={() => setCount(count + 1)}/>
                 <p>You clicked {count} times</p>
            </div>
       
           );
        }
       )