import React, { useState } from 'react';
import { storiesOf } from "@storybook/react";
import NormButton from './NormButton';

export interface State {
    onClick: ()=>void;
}

storiesOf("Normal Button", module)
    .add("Normal Button",
       ()=> {
        const [count, setCount] = useState(0);
        return(
            <div>
                 <NormButton onClick={() => setCount(count + 1)}/>
                 <p>You clicked {count} times</p>
            </div>
       
           );
        }
       )