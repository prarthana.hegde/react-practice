import React from 'react';
const image = require('../images/about-us-page-680x330.jpg');

export default ()=> {
    return(
        <div>
             {/* <h2 style={{padding:'1.5rem',paddingBottom:'1.5rem'}}>About Us</h2> */}
             <img src={String(image)} style={{padding:'1.5rem',paddingBottom:'1.8rem'}}></img>
        <p style={{fontSize:"1.2rem"}}>
                We strive to acheive perfection in our endeavour to train the young minds of our country<br/>
                The website offers a wide range of courses pertaining to interests of the youth of today<br/>
                Get ready for a career in high-demand fields like IT, AI and cloud engineering<br/>
                Upskill your organization with on-demand training and development programs<br/>
                Earn a certificate or degree from a leading university in business, computer science, and more
        </p>
       
        </div>
       
    )
}