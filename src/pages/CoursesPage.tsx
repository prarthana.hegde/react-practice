import React from 'react';

const certificationStyle ={
    margin:'1.5rem',
    padding:'2rem 1.3rem',
    boxShadow:'2px 2px 4px #555'
}



export default ()=> {
    return(
        <div>
             <div style={{display:'flex'}}>
        
        <div style={certificationStyle}>
         < h2>C# Introduction to Coding</ h2>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
            </p>

    </div>

      
        <div style={certificationStyle}>
            < h2>Data Structure</ h2>
            <p>
                Lorem, ipsum dolor sit amet consectetur adipisicing elit. 
            </p>


        </div>
        <div style={certificationStyle} >
            < h2>HTML CSS</ h2>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. 
            </p>
        </div>
    
   </div>

   <div style={{display:'flex'}}>
        
        <div style={certificationStyle}>
         < h2>C++</ h2>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
            </p>

    </div>

      
        <div style={certificationStyle}>
            < h2>PHP</ h2>
            <p>
                Lorem, ipsum dolor sit amet consectetur adipisicing elit. 
            </p>


        </div>
        <div style={certificationStyle} >
            < h2>Javascript and JQuery</ h2>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. 
            </p>
        </div>
    
   </div>

   <div style={{display:'flex'}}>
        
        <div style={certificationStyle}>
         < h2>Asp.Net</ h2>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
            </p>

    </div>

      
        <div style={certificationStyle}>
            < h2>Android Programming</ h2>
            <p>
                Lorem, ipsum dolor sit amet consectetur adipisicing elit. 
            </p>


        </div>
        <div style={certificationStyle} >
            < h2>DBMS</ h2>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. 
            </p>
        </div>
    
   </div>
        </div>
       
    )
}