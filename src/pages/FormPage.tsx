import React,{useState} from 'react';
import DatePickerT from '../components/DatePickerT';
import { useForm } from "react-hook-form";
import { Link} from "react-router-dom";
import {Form,FormGroup,Button,Label,Input,FormText,Container,Col} from 'reactstrap';
import 'react-datepicker/dist/react-datepicker.css';
import FormLabel from '../components/FormLabel';
import FormButton from '../components/FormButton';


export interface IProps {
  Buttontitle?:string;
}

const divStyle={
  paddingLeft:'25rem',
  paddingRight:'28rem',
  
}

const divStyle2={
  marginTop:'.5rem',
  border: '.03px solid grey',
  height:'39.5rem',
  width:'100%'
}

const h2Style = {
  padding:'.6rem',
  paddingRight:'2rem'
}

const labelStyle = {
  width:'25rem',
  paddingTop:'.6rem',
  marginLeft:'2rem',
  marginRight:'5rem'
}


const buttonStyle = {
  width:'25rem',
  height:'2.6rem',
  marginTop:'15rem',
  marginRight:'1rem',
}

const FormPage: React.FC = (props) => {
    const [startDate , setStartDate] = useState(new Date());
        const handleChange =(date : any)=>{
          setStartDate(date);
        }
    return(
      
        <div style={divStyle}>
           <div style={divStyle2}>
           <h2 style={h2Style}>Enter your details</h2>

         <FormLabel title="First Name" type="text" placeholder="Enter First Name" style={labelStyle}/>

        <FormLabel  title="Last Name" type="text" placeholder="Enter Last Name" style={labelStyle} />
        

        <Label style={labelStyle}>Date of Birth</Label><br/>
        <DatePickerT startDate={startDate}  onChange={handleChange} flag={1} style={{paddingLeft:'500rem'}} /><br/>

        <Label style={labelStyle}>Gender</Label><br/>
        <FormLabel type="radio"  style={{position:'absolute',bottom:'18.7rem',left:'39.4rem'}} />
        <Label style={{position:'absolute',bottom:'18rem',left:'39.4rem'}}>Male</Label>
        <FormLabel type="radio" style={{position:'absolute',left:'39.4rem',bottom:'17.3rem'}} />
        <Label style={{position:'absolute',bottom:'16.5rem',left:'39.4rem'}}>Female</Label>

         <Label style={{position:'absolute',bottom:'13.5rem',left:'38.2rem'}}>Area of interest</Label>
         <FormLabel type="radio" style={{position:'absolute',left:'38rem',bottom:'12rem'}} /> 
        <Label style={{position:'absolute',bottom:'11.4rem',left:'38.1rem'}}>Front End Development</Label> 
        <FormLabel type="radio" style={{position:'absolute',left:'38rem',bottom:'10.5rem'}}  />
        <Label style={{position:'absolute',bottom:'9.7rem',left:'38.1rem'}}>Python</Label>
        <FormLabel type="radio" style={{position:'absolute',left:'38rem',bottom:'8.9rem'}}  />
        <Label style={{position:'absolute',bottom:'8rem',left:'38.1rem'}}>Networking</Label> 
        <FormLabel  type="radio" style={{position:'absolute',left:'38rem',bottom:'7rem'}}/> 
        <Label style={{position:'absolute',bottom:'6rem',left:'38.1rem'}}>C,C++,Java</Label>  

        <Link to="/main">
             <FormButton style={buttonStyle} type="submit" Buttontitle="Submit"/>
             </Link>
           </div>
        
  </div>
      
    )
}

export default FormPage;