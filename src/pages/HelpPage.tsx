import React from 'react';
import FormT from '../components/FormT';
import FormButton from '../components/FormButton';
import FormLabel from '../components/FormLabel';

export interface IProps {
    Buttontitle?:string;
  }
const HelpPage: React.FC<IProps> = (props) =>{

    return(
        <div>
            <div>
             <h1 style={{paddingTop:'2rem',paddingBottom:'2rem'}}>CONTACT US</h1>
                <p style={{paddingLeft:'10rem',paddingRight:'10rem',fontSize:'1.3rem'}}>Email us with any questions or inquiries or call 560-098-9909.
                We would be happy to answer your questions and set up a meeting with you.</p>
        </div>

        <div>
        <FormLabel title="Name" type="text" placeholder=" Name"/>
        <FormLabel title="Email" type="text" placeholder="Email"/>
        <FormLabel title="Subject" type="text" placeholder="Subject"/>
        <FormLabel title="Message" type="text" placeholder="Message"/>
        <FormButton Buttontitle="Send Email"/>
        </div>
        </div>
        
       
    )
}

export default HelpPage;