import React from 'react';
import {Link} from "react-router-dom";
const image1 = require('../images/icons8-brainstorm-skill-80.png');
const image2 = require('../images/icons8-classroom-64.png');
const image3 = require('../images/icons8-diploma-64.png');
const image4 = require('../images/icons8-organization-chart-people-50.png');


const MainPage: React.FC = () => {
    return(
        <div>
            <div >
            <nav style={{display:"center"}}>
            <ul style={{display:"flex",marginLeft: '50rem'}}>
                  <li style={{listStyleType: "none",fontSize:'1.7rem',marginRight:'5rem'}}><Link to="/about">About</Link></li>
                <li style= {{listStyleType: "none",fontSize:'1.7rem',marginRight:'5rem'}}><Link to="/courses">Courses</Link></li>
                <li style= {{listStyleType: "none",fontSize:'1.7rem',marginRight:'0rem'}}><Link to="/help">Help</Link></li>
            </ul>
        </nav>
        </div>

        <h1 style={{paddingTop:'5rem',paddingBottom:'4rem'}}>Achieve your goals with us </h1>
        <div style={{display:'flex',margin:'1rem'}}>
        <div>
        <img src={String(image1)}></img>
        <h5>Learn the latest skills</h5>
        <p>like business analytics, graphic design and more</p>
        </div>
        <div>
        <img src={String(image2)}></img>
        <h5>Get ready for a career</h5>
        <p>in high-demand fields like IT, AI and cloud engineering</p>
        </div>
        <div>
        <img src={String(image3)}></img>
        <h5>Earn a certificate or degree</h5>
        <p>from a leading university in business, computer science, and more</p>
        </div>
        <div>
        <img src={String(image4)}></img>
        <h5>Upskill your organization</h5>
        <p>with on-demand training and development programs</p>
        </div>
       
        </div>
        </div>
        
        
    )
}

export default MainPage;