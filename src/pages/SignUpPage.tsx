import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { Link} from "react-router-dom";
import { useForm } from 'react-hook-form';
import FormButton from '../components/FormButton';
import FormT from '../components/FormT';

const divStyle1={
  paddingTop:'5rem',
  paddingLeft:'26rem',
  paddingRight:'30rem',
}



const buttonStyle ={
  width:'30rem',
  height:'2.6rem',
  marginTop:'2rem'
}

const labelStyle ={
  paddingTop:'0.8rem',
  width:'30rem',
  paddingLeft:'0.6rem',
  marginBottom:'.1rem'
}

const  h1Style={
  paddingBottom:'1rem',
  paddingLeft:'.05rem'
}

export default function SignUpPage() {
  
  return (
    <div style={divStyle1}>
      < h1 style={ h1Style}>Sign Up</ h1>
      <FormT style={labelStyle} linkto="/form"/>
    </div>
  );
}